// Gallery

const loaderGallery = document.getElementById('loader-block-gallery');

function load(loader) {
    loader.style.display = 'flex';
    // console.log('+');
}
function loadHidden(loader) {
    loader.style.display = 'none';
}

function startMasonry() {
    $('.image-blocks').imagesLoaded(function () {
        $('.image-blocks').masonry({
            itemSelector: '.item',
            columnWidth: 370,
            gutter: 10,
        });
    });

    $('.image-grid-gallery').imagesLoaded(function () {
        $('.image-grid-gallery').masonry({
            itemSelector: '.block-image-gallery',
            columnWidth: 120,
            gutter: 5,
        });
    });
}

startMasonry();

$(document).ready(function () {
    $('.item').slice(0,8).show();
    $('#load-more-gallery').on('click', function (e) {
        e.preventDefault();
        setTimeout(load, 1, loaderGallery);
        setTimeout(loadHidden, 3000, loaderGallery);
        setTimeout(function () {
            $('.item:hidden').slice(0, 3).fadeIn('slow');
            if($('.item:hidden').length === 0) {
                $('#load-more-gallery').fadeOut('slow');
            }
        }, 3000);
        setTimeout(startMasonry, 3000);
    });
});







