// services Block

const servicesBlock = [
    {
        name: 'Web Design',
        imgUrl: './img/servicesImage/webDesigner.png',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n' +
            'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident,\n' +
            'sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet,\n' +
            'consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident,\n' +
            'sunt in culpa qui officia deserunt mollit anim id est laborum.\n' +
            'Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet,\n' +
            'consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        name: 'Graphic Design',
        imgUrl: './img/servicesImage/web-design1.jpg',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n' +
            'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident,\n' +
            'sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet,\n'

    },
    {
        name: 'Online Support',
        imgUrl: './img/servicesImage/web-design2.jpg',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n' +
            'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n'
    },
    {
        name: 'App Design',
        imgUrl: './img/servicesImage/web-design3.jpg',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n' +
            'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident,\n' +
            'sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet,\n' +
            'consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n'
    },
    {
        name: 'Online Marketing',
        imgUrl: './img/servicesImage/web-design4.jpg',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n'
    },
    {
        name: 'SEO Service',
        imgUrl: './img/servicesImage/web-design5.jpg',
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit,\n' +
            'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident,\n' +
            'sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet,\n' +
            'consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n' +
            'Excepteur sint occaecat cupidatat non proident,\n'
    }
];

const serviceMenu = document.querySelectorAll('.services-filter-block .universal-list-item');
const serviceInfoBlock = document.querySelector('.service-info-block');
// serviceMenu[0].querySelector('a').style.color = 'white';


const image = document.createElement('img');
image.src = servicesBlock[0].imgUrl;
image.style.width = '195px';
image.style.height = '170px';

const serviceText = document.createElement('p');
serviceText.textContent = servicesBlock[0].text;
serviceText.classList.add('service-info-text');


serviceMenu.forEach((item) => {
    item.addEventListener('click', function () {
        const active = this.closest('.universal-list-block').querySelector('.universal-active');
        active.classList.remove('universal-active');
        this.classList.add('universal-active');

        servicesBlock.filter((element) => {
            if(this.dataset.id === element.name) {
                image.src = element.imgUrl;
                serviceText.textContent = element.text;
            }
        });
    })
});

serviceInfoBlock.prepend(image);
serviceInfoBlock.append(serviceText);